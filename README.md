# hgrep

1. Run `stack build` in the root of the project.
2. Run `stack exec hgrep-exe REGEX [FILE]` to search for the REGEX pattern in the given list of FILEs
  - Alternatively you can run the executable directly from the respective path which should be couple of levels deep inside  `.stack-work/install/` (ex. `.stack-work/install/x86_64-linux/lts-10.4/8.2.2/bin/hgrep-exe`
3. Run `stack test` to run the test suite