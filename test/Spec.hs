import qualified RegexTestLib as T
import Regex
import ParserCon

main :: IO ()
main = let basics = T.Basics Atom Seq Alt Rep
           features = [T.Parsing print' prs,
                       T.Set Set,
                       T.Any Any,
                       T.Rep numrep,
                       T.Simplify simplify,
                       T.Many many,
                       T.And Meet, 
                       T.Match matches ]
           prs = ParserCon.parse parseRegexpWithBoundaries
           numrep i mi re = NumRep re i mi
           many x = Seq x (Rep x)
       in T.withFeatures basics features
