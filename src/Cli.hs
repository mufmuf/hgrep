module Cli
    ( cli
    ) where

import System.Environment
import Regex

cli :: IO ()
cli = do
    regex:filenames <- getArgs
    contents <- mapM readFile filenames
    let files = zip filenames contents
        filesWithLinesSplitted = map (fmap lines) files
        filesWithEnumeratedLines = map (fmap $ zip [1..]) filesWithLinesSplitted 
        matchAndPairWithLine str = (str, matchRegex regex str)
        matchList = (map . fmap .  map . fmap) matchAndPairWithLine filesWithEnumeratedLines
        makeGrepResultLines (filename, (lineNo, (str, isMatching)):xs) = 
            if isMatching 
            then (filename ++ "#" ++ show lineNo ++ "\t\t" ++ str) : makeGrepResultLines (filename, xs)
            else makeGrepResultLines (filename, xs)
        makeGrepResultLines (filename, []) = []
    mapM_ putStrLn $ concatMap makeGrepResultLines matchList
