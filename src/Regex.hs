module Regex (
    matchRegex, Regexp(..), matches, print', simplify,
    parseRegexp, parseRegexpWithBoundaries) where

import Data.Char
import Data.List
import Data.Function
import ParserCon

data Regexp = Void
  | Empty
  | Any
  | Atom Char
  | Set [Char]
  | Alt Regexp Regexp
  | Seq Regexp Regexp
  | Rep Regexp
  | Neg Regexp
  | Meet Regexp Regexp
  | NumRep Regexp Int (Maybe Int)
  | StartsWith Regexp
  | EndsWith Regexp
  | Unbounded Regexp
  deriving (Eq, Show)

print' :: Regexp -> String

print' Any = "."
print' (Atom c) = c:[]
print' (Set s) = "[" ++ s ++ "]"
print' (Alt e1 e2) = print' e1 ++ "|" ++ print' e2
print' (Seq e1 e2) = print' e1 ++ print' e2
print' (Rep e) = print' e ++ "*"
print' (Neg e) = print' e ++ "!"
print' (Meet e1 e2) = print' e1 ++ "&" ++ print' e2
print' (NumRep e a b) = print' e ++ "{" ++ show a ++ "," ++ b' ++ "}"
  where b' = case b of
          Just a -> show a
          otherwise -> []
print' (StartsWith e) = "^" ++ print' e
print' (EndsWith e) = print' e ++ "$"
print' (Unbounded e) = print' e

specialChars = "().\\*|+{}[]!^&$"
special c = elem c specialChars

-- Parsing
parseRegexpWithBoundaries :: Parser Char Regexp
parseRegexpWithBoundaries = 
    lit '^' *> parseRegexp <* lit '$'
    <|> pure EndsWith <*> parseRegexp <* lit '$'
    <|> pure StartsWith <* lit '^' <*> parseRegexp
    <|> pure Unbounded <*> parseRegexp

parseRegexp :: Parser Char Regexp
parseRegexp = parseR1
   <|> pure Alt <*> parseR1 <* lit '|' <*> parseRegexp
   <|> pure Meet <*> parseR1 <* lit '&' <*> parseRegexp
   
parseR1 :: Parser Char Regexp
parseR1 = parseR2
  <|> pure Seq <*> parseR2 <*> parseR1

parseR2 = parseAtom
  <|> pure Rep <*> parseAtom <* lit '*'
  <|> pure (\x -> Seq x (Rep x)) <*> parseAtom <* lit '+'
  <|> pure (Alt Empty) <*> parseAtom <* lit '?'
  <|> pure NumRep <*> parseAtom <* lit '{'
                  <*> parseInt <* lit ','
                  <*> maybeParseInt <* lit '}'
  <|> pure Neg <*> parseAtom <* lit '!'
                  
parseAtom :: Parser Char Regexp
parseAtom = pure Atom <*> satisfy (not . special)
  <|> pure Atom <* lit '\\' <*>  satisfy special
  <|> pure Any <* lit '.'
  <|> lit '(' *> parseRegexp <* lit ')'
  <|> pure Set <* lit '[' <*> parseChars <* lit ']'
  <|> pure (Neg . Set) <* lit '[' <* lit '^' <*> parseChars <* lit ']'

{-
genseq :: Regexp -> Integer -> Integer -> Regexp
genseq expr a b
  | (a > b) = Void
  | (a == b) = nseq a expr 
  | otherwise = Alt (nseq a expr) (genseq expr (a+1) b)
  where nseq n expr
          | (n == 0) = Empty
          | otherwise = Seq expr (nseq (n-1) expr)
-}

parseInt :: Parser Char Int
parseInt = read <$> many1 (satisfy isDigit)

maybeParseInt :: Parser Char (Maybe Int)
maybeParseInt = return <$> parseInt
  <|> pure Nothing
  
parseChars :: Parser Char [Char]
parseChars = many (satisfy (not . special) <|> lit '\\' *> satisfy special)

-- Matching
nullable :: Regexp -> Bool
nullable Void = False
nullable Empty = True
nullable Any = False
nullable (Atom _) = False
nullable (Set _) = False
nullable (Alt e1 e2) = nullable e1 || nullable e2
nullable (Seq e1 e2) = nullable e1 && nullable e2
nullable (Rep expr) = True

nullable (Neg expr) = not $ nullable expr 
nullable (Meet e1 e2) = nullable e1 && nullable e2

nullable (NumRep reg n m) = n <= 0 || nullable reg
nullable (StartsWith reg) = nullable reg 
nullable (EndsWith reg) = nullable reg 
nullable (Unbounded reg) = nullable reg 


delta :: Char -> Regexp -> Regexp
delta _ Void = Void
delta _ Empty = Void
delta _ Any = Empty
delta a (Atom b) = if a == b then Empty else Void
delta a (Set b) = if a `elem` b then Empty else Void
delta a (Alt e1 e2) = Alt (delta a e1) (delta a e2)
delta a (Seq e1 e2) = if nullable e1 then
  Alt (Seq (delta a e1) e2) (delta a e2)
  else Seq (delta a e1) e2
delta a (Rep expr) = Seq (delta a expr) (Rep expr)

delta a (Neg expr) = Neg (delta a expr)
delta a (Meet e1 e2) = Meet (delta a e1) (delta a e2)

delta a (NumRep reg n m') = case m' of
  Just m -> if m > 0
            then Seq (delta a reg) (NumRep reg (n - 1) (Just $ m - 1))
            else Void
  otherwise -> Seq (delta a reg) (NumRep reg (n - 1) Nothing)
  

simplify :: Regexp -> Regexp

simplify (Seq Void _) = Void
simplify (Seq _ Void) = Void
simplify (Seq Empty e) = e
simplify (Seq e Empty) = e

simplify (Rep (Rep x)) = Rep x
simplify (Rep Empty) = Empty
simplify (Neg (Neg x)) = x

simplify (Alt Void e) = e
simplify (Alt e Void) = e

simplify (Meet Void _) = Void
simplify (Meet _ Void) = Void

simplify (Alt a b) | a == b = a
simplify (Meet a b) | a == b = a

simplify (Meet (Seq (Atom a) _) (Seq (Atom b) _)) | a /= b = Void

simplify x = x

matches :: Regexp -> String -> Bool
r `matches` [] = nullable r

StartsWith reg `matches` xs  = (Seq reg (Rep Any)) `matches` xs
EndsWith reg `matches` xs = (Seq (Rep Any) reg) `matches` xs
Unbounded reg `matches` xs = (Seq (Rep Any) (Seq reg (Rep Any))) `matches` xs

r `matches` (x:xs) = delta x r' `matches` xs
  where r' = simplify r
  
matchRegex :: String -> String -> Bool
matchRegex regex str = case parse parseRegexpWithBoundaries regex of
    Just regexp -> regexp `matches` str
    Nothing -> False

many1 p = (:) <$> p <*> many p
